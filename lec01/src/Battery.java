
public class Battery {
	
	private int electric = 100;
	
	public int use() {
		if(electric <= 0) {
			
			System.out.println("電池切れ。");
			return 0;
		}
		
		System.out.println("電気を使った。");
		electric -= 10;
		
		return electric;
	}
	
	public int check() {
		
		System.out.println("電気は残り"+electric+"です。");
		return electric;
	}
}

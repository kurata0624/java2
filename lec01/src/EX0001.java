

public class EX0001 {
	
	public static void main(String[] args) {
		boolean ret;
		
		ret = EX0001.isPositive(1);
		System.out.println(ret);
		
		ret = EX0001.isPositive(0);
		System.out.println(ret);
		
		ret =EX0001.isPositive(-1);
		System.out.println(ret);
	}
	public static boolean isPositive(int value) {
		if(value > 0) {
			return true;
		}
		else {
			return false;
		}
	}
}


public class EX0011 {
	public static void main(String[]args) {
		
		Battery battery = new Battery();
		
		battery.check();
		
		battery.use();
		battery.check();
		
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		battery.use();
		
		battery.check();
	}
}

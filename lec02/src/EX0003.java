import produce.Kiwi;
public class EX0003 {
	
	public static void main(String[]args) {
		
		new Kiwi(15.0, false).eat();
		new Kiwi(16.0, false).eat();
		new Kiwi(15.0,true).eat();
		new Kiwi(16.0,true).eat();
	}

}

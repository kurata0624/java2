import creature.Human;
import produce.Tomato;

public class EX0007 {
	
	public static void main(String  args[]) {
		
		Tomato tomato =new Tomato("真っ赤");
		Human  human = new Human("よしお");
		
		human.introduce();
		tomato.hit(human);
	}

}

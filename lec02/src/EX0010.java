import creature.Greengrocer;
import creature.Human;
import produce.Tomato;
import produce.Watermelon;

public class EX0010 {
	public static void main(String[]args) {
		Greengrocer greengrocer = new Greengrocer();
		
		Watermelon watermelon = greengrocer.sellWatermelon();
		watermelon.eat();
		
		Human human = new Human("�悵��");
		Tomato tomato = greengrocer.sellTomato();
		tomato.hit(human);
	}
}

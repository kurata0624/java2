package creature;

public class Const {
	 // EX0007
    public static final String HUMAN_INTRODUCE  = "%sです。";
 
    // EX0010
    public static final double GREENGROCER_WATERMELON_SUGAR_CONTENT = 14.0;
    public static final String GREENGROCER_TOMATO_COLOR  = "真っ黒";
    public static final String GREENGROCER_MSG_SELL_WATERMELON = "スイカをどうぞ。";
    public static final String GREENGROCER_MSG_SELL_TOMATO = "とびっきりのトマトだよ。";
    private Const() {}  // new 防止
}

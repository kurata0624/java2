package creature;
import produce.Tomato;
import produce.Watermelon;

public class Greengrocer {
	
	private Watermelon watermelon;
	private Tomato tomato;
	
	public Greengrocer() {
		this.watermelon = new Watermelon(Const.GREENGROCER_WATERMELON_SUGAR_CONTENT);
		this.tomato = new Tomato(Const.GREENGROCER_TOMATO_COLOR);
	}
	
	public Watermelon sellWatermelon() {
		System.out.println(Const.GREENGROCER_MSG_SELL_WATERMELON);
		return watermelon;
	}
	
	public Tomato sellTomato() {
		System.out.println(Const.GREENGROCER_MSG_SELL_TOMATO);
		return tomato;
	}

}

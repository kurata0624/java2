package creature;

public class Human {
	
	private String name;
	
	public Human(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void introduce() {
		
		String out = String.format(Const.HUMAN_INTRODUCE, name);
		System.out.println(out);
	}
}
package produce;

public class Const {
	 // EX0001
    public static final double  WATERMELON_SUGAR_CONTENT_SWEET = 12.0;
    public static final String  WATERMELON_MSG_SWEET = "甘いスイカだ。";
    public static final String  WATERMELON_MSG_NORMAL = "普通のスイカだ。";
    public static final int         WATERMELON_RET_SWEET = 1;
    public static final int         WATERMELON_RET_NORMAL = 0;
    
    // EX0002
    public static final double  PEACH_SUGAR_CONTENT_SWEET = 15.0;
    public static final String  PEACH_MSG_SWEET = "甘い桃だ。";
    public static final String  PEACH_MSG_NORMAL = "普通の桃だ。";
    public static final int     PEACH_RET_SWEET = 1;
    public static final int         PEACH_RET_NORMAL = 0;
 
    // EX0003
    public static final double  KIWI_SUGAR_CONTENT_SWEET = 16.0;
    public static final String  KIWI_MSG_SWEET = "甘いキウイだ。";
    public static final String  KIWI_MSG_NORMAL = "普通のキウイだ。";
    public static final String  GOLDEN_KIWI_MSG_SWEET = "甘いゴールデンキウイだ。";
    public static final String  GOLDEN_KIWI_MSG_NORMAL = "普通のゴールデンキウイだ。";
    public static final int         KIWI_RET_SWEET = 1;
    public static final int         KIWI_RET_NORMAL = 0;
    
    // EX0004
    public static final String  GRAPE_MSG_EMPTY = "ぶどうはもうない。";
    public static final String  GRAPE_MSG_EAT = "ぶどうを食べた。";
 
    // EX0007
    public static final String  TOMATO_MSG_HIT = "%sはトマトがぶつかり%sに染まった。";
    

    
 
    private Const() {}  // new 防止
}

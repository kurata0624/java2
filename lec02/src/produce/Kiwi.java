package produce;

public class Kiwi {
	
	double sugarContent;
	boolean isGolden;
	
	public Kiwi (double sugarContent, boolean isGolden) {
		this.sugarContent = sugarContent;
		this.isGolden = isGolden;
	}
	public int eat() {
		
		String out;
		if(sugarContent >= Const.KIWI_SUGAR_CONTENT_SWEET) {
			out = isGolden ? Const.GOLDEN_KIWI_MSG_SWEET : Const.KIWI_MSG_SWEET;
			System.out.println(out);
			return Const.KIWI_RET_SWEET;
		}
		
		out = isGolden ? Const.GOLDEN_KIWI_MSG_NORMAL : Const.KIWI_MSG_NORMAL;
		System.out.println(out);
		return Const.KIWI_RET_NORMAL;
	}
}

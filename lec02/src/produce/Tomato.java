package produce;

import creature.Human;

public class Tomato {
	
	private String color;
	
	public Tomato(String color) {
		this.color =color;
	}

	public void hit(Human human) {
		String msg =String.format(Const.TOMATO_MSG_HIT,human.getName(),color);
		System.out.println(msg);
	}
}

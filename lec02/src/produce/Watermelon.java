package produce;

public class Watermelon {
	
	double sugarContent;
	
	public Watermelon(double sugerContent) {
		this.sugarContent = sugerContent;
	}
	
	public int eat() {
	
		if(sugarContent >= Const.WATERMELON_SUGAR_CONTENT_SWEET) {
			System.out.println(Const.WATERMELON_MSG_SWEET);
			return Const.WATERMELON_RET_SWEET;
		}
		
		System.out.println(Const.WATERMELON_MSG_NORMAL);
		return Const.WATERMELON_RET_NORMAL;
		
	}
}

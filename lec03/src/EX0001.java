import car.Car;
import car.Excavator;
import car.FlyingCar;
import car.SuperCar;

public class EX0001 {
	
	public static void main(String[]args) {
		
		Car car = new Car();
		car.run();
		
		SuperCar superCar = new SuperCar();
		superCar.run();
		superCar.turboDash();
		
		FlyingCar flyingCar = new FlyingCar();
		flyingCar.run();
		flyingCar.fly();
		
		Excavator excavator = new Excavator();
		excavator.run();
		excavator.dig();
	}

}

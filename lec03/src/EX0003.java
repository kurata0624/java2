import bird.Bird;
import bird.Chicken;
import bird.Ostrich;
import bird.Penguin;
import bird.Pigeon;
import bird.Sparrow;
public class EX0003 {
	
	public static void main(String args[]) {
		
		Bird bird = new Bird();
		bird.flap();
		
		Chicken chickens = new Chicken();
		chickens.flap();
		chickens.cry();
		
		Pigeon pigeon= new Pigeon();
		pigeon.flap();
		pigeon.cry();
		
		Sparrow sparrow = new Sparrow();
		sparrow.flap();
		sparrow.cry();
		
		Penguin penguin = new Penguin();
		penguin.flap();
		penguin.swim();
		
		Ostrich ostrich = new Ostrich();
		ostrich.flap();
		ostrich.dash();
		
	}
}
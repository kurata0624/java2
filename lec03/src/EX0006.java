import pot.Pot;
import pot.PressureCooker;

public class EX0006 {
	
	public static void main(String args[]) {
		
		Pot pot = new Pot("にんじん");
		pot.boil();
		
		PressureCooker pc = new PressureCooker("ブロッコリー");
		pc.boil();
		pc.press();
	}
}

import ramen.Ramen;
import ramen.TonkotsuRamen;
import ramen. SoySourceRamen;
import ramen.MisoRamen;
public class EX0008 {
	
	public static void main(String args[]) {
		
		Ramen ramen;
		
		ramen = new TonkotsuRamen();
		ramen.info();
		
		ramen = new  SoySourceRamen();
		ramen.info();
		
		ramen = new MisoRamen();
		ramen.info();
	
	}

}

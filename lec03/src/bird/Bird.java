package bird;

public class Bird {
	
	private String name;
	
	public Bird() {
		name = Const.BIRD_NAME;
	}
	
	protected Bird(String name) {
		this.name = name;
	}

	public void flap() {
		System.out.println(String.format(Const.BIRD_FLAP,name));
	}
}

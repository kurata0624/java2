package bird;

public class Chicken extends Bird{

	public Chicken() {
		super(Const.CHICKEN_NAME);
	}
	
	public void cry() {
		System.out.println(Const.CHICKEN_CRY);
	}
}

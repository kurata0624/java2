package bird;

class Const {

    public static final String BIRD_NAME    = "鳥";
    public static final String BIRD_FLAP    = "%sが羽ばたいた。";
 
    public static final String CHICKEN_NAME = "にわとり";
    public static final String CHICKEN_CRY  = "コケコッコー";
 
    public static final String PIGEON_NAME  = "はと";
    public static final String PIGEON_CRY   = "ポッポー";
 
    public static final String SPARROW_NAME = "すずめ";
    public static final String SPARROW_CRY  = "チュンチュン";
 
    public static final String PENGUIN_NAME = "ペンギン";
    public static final String PENGUIN_SWIM = "すいすい";
 
    public static final String OSTRICH_NAME = "だちょう";
    public static final String OSTRICH_DASH = "どたどた";
 
    private Const(){}  // new 防止

}

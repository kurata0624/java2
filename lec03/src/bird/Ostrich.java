package bird;

public class Ostrich extends Bird{
	
	public Ostrich() {
		super(Const.OSTRICH_NAME);
	}
	
	public void dash() {
		System.out.println(Const.OSTRICH_DASH);
	}

}

package bird;

public class Penguin extends Bird{

	public Penguin() {
		super(Const.PENGUIN_NAME);
	}
	
	public void swim() {
		System.out.println(Const.PENGUIN_SWIM);
	}
}

package bird;

public class Pigeon extends Bird{
	
	public Pigeon() {
		super(Const.PIGEON_NAME);
	}

	public void cry() {
		System.out.println(Const.PIGEON_CRY);
	}
}

package bird;

public class Sparrow extends Bird{
	
	public Sparrow() {
		super(Const.SPARROW_NAME);
	}
	
	public void cry() {
		System.out.println(Const.SPARROW_CRY);
	}

}

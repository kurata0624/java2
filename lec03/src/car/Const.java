package car;

public class Const {
	 public static final String CAR_RUN   = "車が走った。";
	 
	    public static final String SUPERCAR_TURBODASH = "スーパーカーが猛烈に加速した。";
	 
	    public static final String FLYINGCAR_FLY   = "車が空を飛んだ。";
	 
	    public static final String EXCAVATOR_DIG   = "ショベルカーは掘った。";
	 
	    private Const(){}  // new 防止
	

}

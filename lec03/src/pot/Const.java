package pot;
 
class Const {
 
    public static final String POT_BOIL = "%sをゆでた。";
 
    public static final String PRESSURE_COOKER_PRESS = "%sに圧力をかけた。";
 
    private Const(){}  // new 防止
}
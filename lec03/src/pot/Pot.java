package pot;

public class Pot {
	
	protected String inside;
	
	public Pot(String inside) {
		this.inside = inside;
	}

	public void boil() {
		String msg = String.format(Const.POT_BOIL, this.inside);
		System.out.println(msg);
	}
}

package pot;

public class PressureCooker extends Pot{
	
	public PressureCooker(String inside) {
		super(inside);
	}

	public void press() {
		String msg = String.format(Const.PRESSURE_COOKER_PRESS,super.inside);
		System.out.println(msg);
	}
}

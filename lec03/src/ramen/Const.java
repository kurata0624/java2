package ramen;
 
class Const {
 
    public static final String NOODLE = "麺";
 
    public static final String TONKOTSU_RAMEN = "とんこつラーメン";
    public static final String TONKOTSU_SOUP  = "とんこつスープ";
 
    public static final String SOY_SOURCE_RAMEN = "しょうゆラーメン";
    public static final String SOY_SOURCE_SOUP  = "しょうゆスープ";
 
    public static final String MISO_RAMEN = "みそラーメン";
    public static final String MISO_SOUP  = "みそスープ";
 
    public static final String RAMEN_INFO  = "%sは%sに%sを入れた食品です。";
 
    private Const(){}  // new 防止
}
package ramen;

public class Ramen {
	
	private String name;
	private String noodle;
	private String soup;
	
	protected Ramen(String name,String soup) {
		this.name = name;
		noodle = Const.NOODLE;
		this.soup = soup;
	}

	public void info() {
		System.out.println(String.format(Const.RAMEN_INFO, name, soup,noodle));
	}
}

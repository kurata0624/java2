import ex0007.WaterBaloon;
import ex0007.WaterTooMuchException;

public class EX0007 {
	
	public static void main(String args[]) {
		WaterBaloon waterBaloon = new WaterBaloon();
		try {
			waterBaloon.putWater();
			waterBaloon.putWater();
			waterBaloon.putWater();
			waterBaloon.putWater();
			waterBaloon.putWater();
			
			waterBaloon.putWater();
			waterBaloon.putWater();
			waterBaloon.putWater();
			waterBaloon.putWater();
			waterBaloon.putWater();
			
			waterBaloon.putWater();
			
		}
		catch(WaterTooMuchException e) {
			
			System.out.println(e.getMessage());
		}
	}

}

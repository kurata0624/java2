import ex0011.Tour;
import ex0011.Tourist;

public class EX0011 {
	public static void main(String args[]) {
		Tourist tourist = new Tourist();
		
		Tour tour = new Tour(tourist);
		tour.tourMtFuji();
	}

}

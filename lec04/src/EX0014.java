import ex0014.Faucet;
import ex0014.FaucetBrokenException;
import ex0014.FaucetFrozenException;

public class EX0014 {
	public static void main(String args[]) {
		
		Faucet faucet = new Faucet();
		
		for(int i = 0; i < 5; i++) {
			
			try{
			faucet.open();
			}
			catch(FaucetBrokenException fbe) {
				System.out.println(fbe.getMessage());
				faucet.repair();
			}
			catch(FaucetFrozenException fbe) {
				System.out.println(fbe.getMessage());
				faucet.warm();
		
			}	
		}
	}
}

import ex0021.DysonFan;
import ex0021.Fan;
import ex0021.HugeFan;
import ex0021.Mouth;

public class EX0021 {
	public static void main(String args[]) {

		Fan[] fans = new Fan[4];
		fans[0] = new Fan();
		fans[1] = new HugeFan();
		fans[2] = new DysonFan();
		fans[3] = new Mouth();
		
		for (Fan fan : fans) {
			
			fan.blow();
		}
	
		
	}
}

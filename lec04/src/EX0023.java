import ex0023.Juice;
import ex0023.MelonJuice;
import ex0023.LemonJuice;
import ex0023.RedPepperJuice;

public class EX0023 {
	public static void main(String args[]) {
		
		Juice[] juices = new Juice[4];
		juices[0] = new Juice();
		juices[1] = new MelonJuice();
		juices[2] = new LemonJuice();
		juices[3] = new RedPepperJuice();
		
		for(Juice juice : juices) {
			
			juice.quenchThirst();
		}
	}
}

import ex0026.Car;
import ex0026.Truck;
import ex0026.SuperCar;
import ex0026.Clunker;

public class EX0026 {
	
	public static void main(String args[]) {

		Car[] cars = new Car[4];
		
		cars[0] = new Car();
		cars[1] = new Truck();
		cars[2] = new SuperCar();
		cars[3] = new Clunker();
		
		for(Car car: cars) {
			
			car.run();
		}
	}

}

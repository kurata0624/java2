package ex0007;
 
class Const {
 
    public static final int WATER_CAPACITY = 10;
 
    public static final String MSG_PUT_WATER = "水を入れた。";
 
    public static final String MSG_WATER_BALOON_FULL = "水がいっぱい。";
 
    private Const(){}  // new 防止
}
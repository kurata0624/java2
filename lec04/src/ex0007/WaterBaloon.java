package ex0007;

public class WaterBaloon {
	private int waterAmount;
	
	public WaterBaloon() {
		waterAmount = 0;
	}
	
	public void putWater() throws WaterTooMuchException{
		
		waterAmount++;
		
		System.out.println(Const.MSG_PUT_WATER);
		
		if(waterAmount > Const.WATER_CAPACITY) {
			
			throw new WaterTooMuchException(Const.MSG_WATER_BALOON_FULL);
		}
	}
}

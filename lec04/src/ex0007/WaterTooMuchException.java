package ex0007;

public class WaterTooMuchException extends Exception{
	public WaterTooMuchException(String msg) {
		super(msg);
	}
}

package ex0011;
 
class Const {
 
    public static final String TOUR_MT_FUJI = "富士山ツアーです。";
 
    public static final String MOUNTAIN_ERUPT = "山が噴火している。";
 
    public static final String TOUR_EMERGENCY_STOP = "ツアー中止。みんな逃げて。";
 
    private Const(){}  // new 防止
}
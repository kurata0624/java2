package ex0011;

public class MountainEruptionException extends RuntimeException {
	public MountainEruptionException(String msg) {
		super(msg);
	}
}

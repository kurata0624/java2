package ex0011;

public class Tour {
	private Tourist tourist;
	public Tour(Tourist tourist) {
		this.tourist = tourist;
	}
	public void tourMtFuji() {
		System.out.println(Const.TOUR_MT_FUJI);
		
		try {
			tourist.seeMtFuji();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println(Const.TOUR_EMERGENCY_STOP);
			}
		}
}

package ex0014;
 
class Const {
 
    public static final String REPAIR = "直した。";
 
    public static final String WARM = "温めた。";
 
    public static final String WATER_RUN = "水が出た。";
 
    public static final String FAUCET_BROKEN = "壊れている。";
 
    public static final String FAUCET_FROZEN = "凍っている。";
 
    private Const(){}  // new 防止
}
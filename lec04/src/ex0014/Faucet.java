package ex0014;

public class Faucet {
	
	private boolean isBroken;
	private boolean isFrozen;
	
	public Faucet() {
		isBroken = true;
		isFrozen = true;
	}

	public void repair() {
		
		System.out.println(Const.REPAIR);
		isBroken = false;
	}
	
	public void warm() {
			System.out.println(Const.WARM);
			isFrozen = false;
	}
	public void open() throws FaucetBrokenException,FaucetFrozenException{
		if(isBroken) throw new FaucetBrokenException();
		if(isFrozen) throw new FaucetFrozenException();
	
		System.out.println(Const.WATER_RUN);
	}
	
	
}
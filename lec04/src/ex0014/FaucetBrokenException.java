package ex0014;

public class FaucetBrokenException extends Exception {
	
	public FaucetBrokenException() {
		super(Const.FAUCET_BROKEN);
	}
	
}

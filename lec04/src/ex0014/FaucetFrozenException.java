package ex0014;

public class FaucetFrozenException extends Exception{
	
	public FaucetFrozenException() {
		super(Const.FAUCET_FROZEN);
	}

}

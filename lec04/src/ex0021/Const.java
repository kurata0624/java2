package ex0021;

public class Const {

    public static final String FAN_BLOW      = "風を起こした。";
 
    public static final String MOUTH_BLOW    = "息吹を起こした。";
 
    public static final String DYSONFAN_BLOW = "温風を起こした。";
 
    public static final String HUGEFAN_BLOW  = "暴風を起こした。";
 
    private Const(){}  // new 防止
}

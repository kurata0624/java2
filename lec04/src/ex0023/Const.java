package ex0023;
 
class Const {
 
    public static final String JUICE_NAME            = "ジュース";
    public static final String MELON_JUICE_NAME      = "メロンジュース";
    public static final String LEMON_JUICE_NAME      = "レモンジュース";
    public static final String RED_PEPPER_JUICE_NAME = "とうがらしジュース";
 
    public static final String QUENCH_THIRST = "%sがのどを潤した。";
 
    public static final String SWEET = "甘い。";
    public static final String SOUR  = "すっぱい。";
    public static final String HOT   = "辛い。";
 
    private Const(){}  // new 防止
}
package ex0023;

public class Juice {
	private String name;
	
	public Juice() {
		
		this(Const.JUICE_NAME);
	}
	
	protected Juice(String name) {
		
		this.name = name;
	}
	
	public void quenchThirst() {
		
		String msg = String.format(Const.QUENCH_THIRST, name);
		System.out.println(msg);
	}

}

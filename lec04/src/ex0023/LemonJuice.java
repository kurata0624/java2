package ex0023;

public class LemonJuice extends Juice {
	
	public LemonJuice() {
		
		super(Const.LEMON_JUICE_NAME);
	}
	
	public void quenchThirst() {
		
		super.quenchThirst();
		
		System.out.println(Const.SOUR);
	}
}

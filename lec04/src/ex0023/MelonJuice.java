package ex0023;

public class MelonJuice extends Juice {
	
	public MelonJuice() {
		
		super(Const.MELON_JUICE_NAME);
	}
	
	public void quenchThirst() {
		
		super.quenchThirst();
		
		System.out.println(Const.SWEET);
	}

}

package ex0023;

public class RedPepperJuice extends Juice{
	
	public RedPepperJuice() {
		
		super(Const.RED_PEPPER_JUICE_NAME);
	}
	
	public void quenchThirst() {
		
		super.quenchThirst();
		
		System.out.println(Const.HOT);
	}

}

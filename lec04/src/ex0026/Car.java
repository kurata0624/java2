package ex0026;

public class Car {
	private String name;
	
	public Car() {
		
		this(Const.NAME_CAR);
	}
	protected Car(String name) {
		
		this.name = name;
	}
	
	public void run() {
		
		System.out.println(String.format(Const.CAR_RUN, this.name,this.appearance()));
	}
	public String appearance() {
		
		return Const.APPEARANCE_CAR;
	}
}

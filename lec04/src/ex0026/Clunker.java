package ex0026;

public class Clunker extends Car{
	
	public Clunker() {
		
		super(Const.NAME_CLUNKER);
	}
	
	public String appearance() {
		
		return Const.APPEARANCE_CLUNKER;
	}

}

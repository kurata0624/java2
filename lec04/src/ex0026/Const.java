package ex0026;
 
class Const {
 
    public static final String CAR_RUN = "%sが%s走る。";
 
    public static final String NAME_CAR = "車";
 
    public static final String NAME_TRUCK = "トラック";
 
    public static final String NAME_SUPERCAR = "スーパーカー";
 
    public static final String NAME_CLUNKER = "ポンコツ車";
 
    public static final String APPEARANCE_CAR = "普通に";
 
    public static final String APPEARANCE_TRUCK = "ぶんぶん";
 
    public static final String APPEARANCE_SUPERCAR = "びゅんびゅん";
 
    public static final String APPEARANCE_CLUNKER = "のろのろ";
 
    private Const(){}  // new 防止
}
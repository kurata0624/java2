package ex0026;

public class SuperCar extends Car{
	
	public SuperCar() {
		
		super(Const.NAME_SUPERCAR);
	}
	
	public String appearance() {
		
		return Const.APPEARANCE_SUPERCAR;
	}

}

package ex0026;

public class Truck extends Car{
	
	public Truck(){
		
		super(Const.NAME_TRUCK);
	}
	public String appearance() {
		
		return Const.APPEARANCE_TRUCK;
	}
}

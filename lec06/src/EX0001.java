import ex0001.Car;
import ex0001.Truck;

public class EX0001 {
	
	public static void main(String args[]) {
		
		Car[] cars = new Car[3];
		cars[0] = new Truck();
		
		for(Car car : cars) {
			car.run();
			car.sound();
		}
	}
}

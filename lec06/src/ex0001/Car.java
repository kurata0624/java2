package ex0001;

public abstract class Car {
	
	private String name;
	
	protected Car(String name) {
		
		this.name = name;
	}
	
	public void run() {
		System.out.println(String.format(Const.CAR_RUN, this.name));
	}
	abstract public void sound();

}

package ex0001;
 
public class Const {
 
    public static final String CAR_RUN = "%sが走る。";
 
    public static final String SUPER_CAR_NAME = "スーパーカー";
    public static final String TRUCK_NAME = "トラック";
    public static final String ELECTRIC_CAR_NAME = "電気自動車";
 
    public static final String SUPER_CAR_SOUND = "びゅーん。";
    public static final String TRUCK_SOUND = "ぶろろろ。";
    public static final String ELECTRIC_CAR_SOUND = "すいー。";
 
    private Const(){}  // new 防止
}
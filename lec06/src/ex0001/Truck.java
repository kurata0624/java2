package ex0001;

public class Truck extends Car{
	
	public Truck() {
		super(Const.TRUCK_NAME);
	}
	@Override
	public void sound() {
		System.out.println(Const.TRUCK_SOUND);
	}
}
